FROM fedora:34
RUN dnf install -y python3-pip openssh-clients && dnf clean all && rm -rf /var/cache/yum
RUN pip install ansible
